---
title: "Matrix an der THM"
date: 2020-08-02T21:26:25+02:00
chapter: false
draft: false
---
{{% notice info %}}
Der Matrix Server der THM wurde aktualisiert (28.03.2021). Falls Sie keine Profil-/Raumbilder oder andere gesendete Medien sehen können, so befolgen Sie bitte folgende Hinweise: [Client neu anmelden]({{< relref "faq/#relogin" >}})
{{% /notice %}}

## Matrix an der THM
Matrix ist ein freies und offenes, sicheres, dezentralisiertes Protokoll für Echtzeit-Kommunikation, das auch unter dem Namen eines seiner nutzenden Programme, Element, bekannt ist.

<object data="./images/matrix_interactive.svg" type="image/svg+xml" style="width: 1280px; max-width: 100%"></object>

Zur Zusammenarbeit in Teams stieg in den letzten Jahren der Bedarf an unterstützenden digitalen Werkzeugen (engl. *tool*). Ein zentrales Werkzeug ist dabei ein Team-Chat. Ein Chat bezeichnet, laut Wikipedia, „die elektronische Kommunikation mittels geschriebenem Text in Echtzeit, meist über das Internet“ ([Quelle](https://de.wikipedia.org/wiki/Chat)). Die dazugehörige Handlung nennt man „*chatten*“. Mit einem Chattool (manchmal auch Messenger genannt) können sich Teammitglieder gegenseitig auf aktuelle Informationen aufmerksam machen und insb. Verknüpfungen (*Hyperlinks* / *Links*) zur weitergehenden Zusammenarbeit teilen (bspw. zur Terminfindung, zum kollaborativen Schreiben, zum Planen von Events, zum Bearbeiten von Daten, Code, Mindmaps oder Prozessen). Viele Teams an der THM haben sich aufgrund des bisher fehlenden zentralen Angebots eigene Lösungen gesucht, die zum Teil datenschutzbedenklich sind oder nicht mit anderen Systemen verknüpfbar sind.

Zur Deckung des Bedarfes an Echtzeitkommunikation wurde daher Ende 2018, nach vergleichender Analyse mehrerer potentieller Lösungsoptionen, innerhalb der THM das offene Kommunikationsprotokoll Matrix in einem Pilotbetrieb eingeführt und im April 2019 im CIO präsentiert. Im Herbst 2019 wurde der Pilotbetrieb in den Regelbetrieb überführt. U.a. in der [digitalen](https://invidious.13ad.de/AtkA-sE-9uU) [Lehre](https://invidious.13ad.de/jEvKdFTKSxU) (Videos von Prof. Lasch) findet Matrix als Ergänzung zu [OPAL](https://bildungsportal.sachsen.de/opal) Anwendung.

<img id="image-id" style="width=60%; margin-left:0;">
<script>
var cssSelector = "#image-id";
var imageFolderPath = "./images/statements";
var imageCount = 19;
var displayTime = 30000; //in ms
document.querySelector(cssSelector).src = imageFolderPath+"/"+Math.floor(Math.random() * imageCount)+".jpg";
setInterval(() => {
    document.querySelector(cssSelector).src = imageFolderPath+"/"+Math.floor(Math.random() * imageCount)+".jpg";
}, displayTime);
</script>

## Themen der Dokumentation

* [Warum Matrix und kein anderes Chat-System?]({{< relref "why/_index.md" >}})
* [Wie kann Matrix genutzt werden? (Anmeldung und erste Schritte)]({{< relref "first-steps/_index.md" >}})

* [Empfehlungen zu weiteren wichtigen Einstellungen nach dem Erstlogin]({{< relref "settings/_index.md" >}})

* [Installation eines Clients / Programms]({{< relref "clients/_index.md" >}})

    * [Browsereinstellungen]({{< relref "clients/browser/_index.md" >}})

* [Personen finden und direkte Nachrichten versenden]({{< relref "messaging/_index.md" >}})

* [Ende-zu-Ende-Verschlüsselung nutzen]({{< relref "encryption/_index.md" >}})

* [Räume erstellen und Verantwortung übernehmen]({{< relref "rooms/_index.md" >}})

    * [Räume teilen und publik machen]({{< relref "rooms/sharing/_index.md" >}})

    * [Räume finden]({{< relref "rooms/find/_index.md" >}})

    * [Räume löschen und aus Räumen austreten]({{< relref "rooms/delete/_index.md" >}})

* [Benachrichtigungen feiner einstellen]({{< relref "notifications/_index.md" >}})

* [Integrations, Bridges, Bots nutzen (u.a. Jitsi)]({{< relref "integrations/_index.md" >}})

* [Weitere Clients]({{< relref "clients/more_clients/_index.md" >}})

* [Communities als Raum-Filter einsetzen]({{< relref "communities/_index.md" >}})

* [Weiterentwicklung von Matrix]({{< relref "development/_index.md" >}})

* [Datenschutzerklärung]({{< relref "privacy/_index.md" >}})

* [Impressum]({{< relref "imprint/_index.md" >}})

* [Barrierefreiheitserklärung]({{< relref "accessibility/_index.md" >}})

### Danksagung für die Grundlage dieser Dokumentation
Die Grundlage dieser Dokumentation ist die Dokumentation der [TUD](https://doc.matrix.tu-dresden.de/) die dankenwerter Weise unter einer Creative Commons Lizenz veröffentlicht wurde. 
Die Quellen der TUD finden Sie hier [github](https://github.com/matrix-tu-dresden-de/Dokumentation).

Die Quellen der Dokumentation der THM sind in [gitlab](https://git.thm.de/matrix/Dokumentation) zu finden. 
Alle Änderungen die gemacht wurden, um die Dokumentation für die THM anzupassen finden Sie hier in [gitlab](https://git.thm.de/matrix/Dokumentation/-/tree/thm-main).
