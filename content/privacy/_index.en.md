---
title: "Privacy"
date: 2020-07-03T13:25:27+02:00
draft: false
chapter: true
weight: 900
---
## Privacy Policy for the Matrix Service of THM

**What is the purpose of the data processing?**

"Matrix" is an open, decentralized communication service for real-time communication. In compliance with the relevant legal and statutory provisions on data protection and IT security, members and affiliates of the THM are enabled to communicate with members of this and other universities and other Matrix users (e.g. academic partners) via chat and audio/video telephony using their THM UID. The aim of using groupware systems is to ensure and simplify organizational measures for the joint work of users, groups of people, teams and committees as well as communication management. Personal data is processed exclusively for the above-mentioned purposes.

**Who is responsible for data processing and to whom can turn the affected person?**

Responsible
in the sense of Art. 4 No. 7 DSGVO is the [THM](https://thm.de/impressum).

contact person:

TBD

[matrix@thm.de](mailto:matrix@thm.de)

and the data protection officer of the THM

E-Mail: [datenschutz@thm.de](mailto:datenschutz@thm.de)
(https://www.thm.de/datenschutz/)

**Which personal data are processed?**

The processing includes the following personal data:

1. access management: first & last name(s), mail address, Matrix-ID (localpart of
    mail address), display name

2. authentication: username and password

3. user content: all data that the user enters into the system (end-to-end encryption possible)

4. device identification: IP addresses with time stamp and device name; type of device used (mobile / desktop), operating system

5. server log: IP addresses with timestamp

6. audio/video telephony: IP addresses, AV data

7. notifications (mail)

** How long will the personal data be stored?

The personal data is stored according to § 16 Abs. 4 IT-Ordnung of the TU
Dresden at the latest 15 months after the departure of the affected
Person deleted.

**What rights are the persons concerned basically entitled to?**

<u>Right to information (Art. 15 DSGVO)</u>

The parties concerned have the right to obtain information at any time about the
and the possible recipients of this data.
to be able to request data.

<u>Right to correction, deletion and restriction (Art. 16 - 18 DSGVO)</u>

The persons concerned can inform the THM about the
Correction, deletion of your personal data or the
to demand restriction of the processing.

<u>right to data transferability (Art. 20 DSGVO)</u>

The persons concerned can demand that the person responsible give them their
personal data is transmitted in machine-readable format.

<u>right of appeal (Art. 77 DSGVO)</u>

Affected persons can contact the competent supervisory authority at any time
for data protection. The competent supervisory authority is:

Hessischer Datenschutzbeauftragter

TBD

TBD

Tel.: TBD

Fax: TBD

E-Mail: [TBD](mailto:TBD)

[Imprint]({{< ref "imprint" >}})


